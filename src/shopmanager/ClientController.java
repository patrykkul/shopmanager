/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package shopmanager;

import javax.swing.JOptionPane;
import javax.swing.JTable;
import javax.swing.table.DefaultTableModel;

/**
 *
 * @author Patryk
 */
public class ClientController {

    private ClientAddView AddView;
    private ClientEditView EditView;
    private ClientModel client;
    
    private JTable ClientTable;
    
    private int ClientSelectedId = -1;
    
    public ClientController(ClientAddView view, ClientModel client,JTable ClientTable)
    {
        this.ClientTable = ClientTable;
        
        this.client = client;
        
        this.AddView = view;
        
        AddClient_InitController();
    }
    
    public ClientController(ClientEditView view, ClientModel client,JTable ClientTable)
    {
        this.ClientTable = ClientTable;
        
        this.client = client;
        
        this.EditView = view;
        
        ClientSelectedId = ClientTable.getSelectedRow();
        
        EditClient_InitController();
    }
    
 public void AddClient_InitController() {
        AddView.GetAcceptButton().addActionListener(e -> AddClient_Accept());
        AddView.GetCancelButton().addActionListener(e -> AddClient_Cancel());
    }
 
 private void AddClient_Accept()
 {
       AddClient();
 }
 
 private void AddClient_Cancel()
 {
     AddView.getframe().dispose();
 }
 
 private void AddClient()
 {
    DefaultTableModel model = (DefaultTableModel) ClientTable.getModel();
    model.addRow(new Object[]{AddView.getLastNameField().getText(),AddView.getFirstNameField().getText(),AddView.getAddressField().getText()});
    AddView.getframe().dispose();
 }
 
  public void EditClient_InitController() {
        EditView.GetAcceptButton().addActionListener(e -> EditClient_Accept());
        EditView.GetCancelButton().addActionListener(e -> EditClient_Cancel());
    }
 
 private void EditClient_Accept()
 {
       EditClient();
 }
 
 private void EditClient_Cancel()
 {
     EditView.getframe().dispose();
 }
 
 private void EditClient()
 {
     if(ClientSelectedId != -1)
     {
         DefaultTableModel model = (DefaultTableModel) this.ClientTable.getModel();
        int[] rows = ClientTable.getSelectedRows();
        for(int i=0;i<rows.length;i++){
          model.removeRow(rows[i]-i);
        }
        model.insertRow(ClientSelectedId, new Object[]{0, EditView.getLastNameField().getText(),EditView.getFirstNameField().getText(),EditView.getAddressField().getText()});
        EditView.getframe().dispose();
    }
 }
}
